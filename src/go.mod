module softwise

go 1.13

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible
	github.com/miolini/datacounter v1.0.2
	github.com/sirupsen/logrus v1.4.2
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
	golang.org/x/net v0.0.0-20200202094626-16171245cfb2
	gopkg.in/tucnak/telebot.v2 v2.0.0-20200209123123-209b6f88caa9
)
