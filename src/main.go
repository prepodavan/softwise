package main

import (
	"github.com/sirupsen/logrus"
	"net/http"
	"os"
	"softwise/requester"
	"softwise/tg"
)

func init() {
	logrus.SetFormatter(&logrus.JSONFormatter{})
}

func main() {
	//configPath := flag.String("config", "./config.toml", "app config")
	//flag.Parse()

	config := tg.Config{
		Bot: struct {
			AdminID     int
			Token       string
			URL         string
			PollTimeout int
		}{
			AdminID: 628821701,
			Token: os.Getenv("BOTTOKEN"),
			URL: "",
			PollTimeout: 1,
		},
	}

	//if _, err := toml.DecodeFile(*configPath, &config); err != nil {
	//	logrus.WithFields(logrus.Fields{
	//		"tag": "config parsing",
	//		"data": logrus.Fields{
	//			"config path": *configPath,
	//			"config":      config,
	//			"err": logrus.Fields{
	//				"text": err.Error(),
	//			},
	//		},
	//	}).Fatal()
	//}

	go http.ListenAndServe("0.0.0.0:" + os.Getenv("PORT"), nil)
	bot, _ := tg.NewBot(&config, requester.NewDownloader(nil), new(http.Client))
	bot.Run()
}
