package tg

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"golang.org/x/net/proxy"
	"gopkg.in/tucnak/telebot.v2"
	"net/http"
	"sync"
	"time"
)

type Downloader interface {
	ContentLength(url string) (int64, error)
	ContentLengthAll(numThreads uint64, urls ...string) *sync.Map
}

type Bot struct {
	Api        *telebot.Bot
	Config     *Config
	Downloader Downloader
	Client     *http.Client
}

func (b *Bot) init() (err error) {
	logrus.WithFields(logrus.Fields{
		"tag": "bot-initializing",
		"data": logrus.Fields{
			"Config": *b.Config,
		},
	}).Info()

	if b.Client == nil {
		if err = b.setDefaultClient(); err != nil {
			initError(err)
			return
		}
	}

	b.Api, err = telebot.NewBot(telebot.Settings{
		URL:    b.Config.Bot.URL,
		Token:  b.Config.Bot.Token,
		Poller: &telebot.LongPoller{Timeout: time.Duration(b.Config.Bot.PollTimeout) * time.Second},
		Client: b.Client,
	})

	if err != nil {
		initError(err)
		return
	}

	b.Api.Handle(telebot.OnDocument, b.mainHandler)
	b.Api.Handle("/start", b.start)
	b.Api.Handle("/theory", b.theory)
	return
}

func (b *Bot) setDefaultClient() error {
	socksUrl := fmt.Sprintf("%s:%d", b.Config.Socks5.Host, b.Config.Socks5.Port)
	dialSocksProxy, err := proxy.SOCKS5(b.Config.Socks5.Network, socksUrl, &proxy.Auth{
		User:     b.Config.Socks5.Auth.User,
		Password: b.Config.Socks5.Auth.Password,
	}, proxy.Direct)

	if err != nil {
		return err
	}

	b.Client = &http.Client{
		Transport: &http.Transport{Dial: dialSocksProxy.Dial},
	}

	return nil
}

func (b *Bot) SetConfig(c *Config) error {
	b.Config = c
	return b.init()
}

func (b *Bot) Run() {
	logrus.Info("starting")
	b.Api.Start()
}

func NewBot(config *Config, downloader Downloader, client *http.Client) (*Bot, error) {
	botWrapper := &Bot{Downloader: downloader, Client: client}
	return botWrapper, botWrapper.SetConfig(config)
}
