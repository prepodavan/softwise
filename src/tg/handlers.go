package tg

import (
	"gopkg.in/tucnak/telebot.v2"
	"strings"
)

func (b *Bot) mainHandler(msg *telebot.Message) {
	logNewCountRequest(msg)
	if res, err := b.Api.Reply(msg, "Im processing ur data. please, stay by"); err != nil {
		sendError(err, res, msg)
		return
	}

	req, err := b.mainValidator(msg)
	if err != nil {
		text := "Sorry, smth rly bad happened processing ur data"
		if converted, ok := err.(*ValidationError); ok {
			validationError(converted, msg)
			text = converted.UserMessage
		} else {
			internalError(err, msg)
		}

		if res, err := b.Api.Reply(msg, text); err != nil {
			sendError(err, res, msg)
		}

		return
	}

	var text string
	if text = b.buildReplyText(b.Downloader.ContentLengthAll(uint64(req.NumThreads), req.Urls...)); len(text) == 0 {
		text = "None of ur urls processed without errors"
	}

	if res, err := b.Api.Reply(msg, text); err != nil {
		sendError(err, res, msg)
	}

	return
}

func (b *Bot) start(msg *telebot.Message) {
	if msg.Sender.ID == b.Config.Bot.AdminID && len(msg.Text) > len("/start") {
		b.Config.StartText = strings.Replace(msg.Text, "/start", "", 1)
	}

	if res, err := b.Api.Send(msg.Chat, b.Config.StartText); err != nil {
		sendError(err, res, msg)
	}
}

func (b *Bot) theory(msg *telebot.Message) {
	if msg.Sender.ID == b.Config.Bot.AdminID && len(msg.Text) > len("/theory") {
		b.Config.TheoryText = strings.Replace(msg.Text, "/theory", "", 1)
	}

	if res, err := b.Api.Send(msg.Chat, b.Config.TheoryText); err != nil {
		sendError(err, res, msg)
	}
}
