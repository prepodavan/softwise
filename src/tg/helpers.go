package tg

import (
	"fmt"
	"strings"
	"sync"
)

func (b *Bot) buildReplyText(contentLengths *sync.Map) string {
	var builder strings.Builder
	contentLengths.Range(func(key, value interface{}) bool {
		url := key.(string)
		length := value.(int64)
		builder.WriteString(fmt.Sprintf("%s - %d\n", url, length))
		return true
	})

	if builder.Len() == 0 {
		return ""
	}

	return builder.String()[:builder.Len()-1]
}
