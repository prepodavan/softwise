package tg

import (
	"gopkg.in/tucnak/telebot.v2"
	"io"
	"net/url"
	"strconv"
	"strings"
)

func (b *Bot) mainValidator(msg *telebot.Message) (req *UserRequest, err error) {
	if msg.Document == nil {
		err = &ValidationError{
			text:        "requires file with urls",
			UserMessage: "Ur file is invalid. I require file with each line contained URLs only",
		}
		return
	} else if threads, atoiErr := strconv.Atoi(strings.Trim(msg.Caption, "\n\t \r")); atoiErr != nil || threads < 1 {
		err = &ValidationError{
			text:        "invalid num of threads",
			UserMessage: "U should specify positive number of threads that will process ur URLs",
		}
		return
	} else {
		var urls []string
		urls, err = b.fileContentValidator(&msg.Document.File)
		if err != nil {
			return
		}

		req = new(UserRequest)
		req.NumThreads = threads
		req.Urls = urls
	}

	return
}

func (b *Bot) fileContentValidator(file *telebot.File) (urls []string, err error) {
	reader, getErr := b.Api.GetFile(file)
	if getErr != nil {
		err = getErr
		return
	}

	defer reader.Close()
	buf := make([]byte, file.FileSize)
	_, readErr := reader.Read(buf)
	if readErr != nil && readErr != io.EOF {
		err = readErr
		return
	}

	for _, line := range strings.Split(strings.Replace(string(buf), "\r", "", -1), "\n") {
		parsed, parseErr := url.Parse(line)
		if parseErr != nil {
			continue
		}

		urls = append(urls, parsed.String())
	}

	if len(urls) == 0 {
		err = &ValidationError{
			text:        "no urls",
			UserMessage: "Ur file is empty. It should contain URLs on each lines",
		}
	}

	return
}
