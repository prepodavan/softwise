package tg

type ValidationError struct {
	internal          error
	text, UserMessage string
}

func (v *ValidationError) Error() string {
	if v.internal == nil {
		return v.text
	}

	return v.internal.Error()
}
