package tg

type Config struct {
	TheoryText, StartText string
	Bot                   struct {
		AdminID     int
		Token       string
		URL         string
		PollTimeout int
	}
	Socks5 struct {
		Auth struct {
			User     string `json:"-"`
			Password string `json:"-"`
		}
		Host    string
		Port    uint64
		Network string
	}
}
