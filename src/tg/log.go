package tg

import (
	"github.com/sirupsen/logrus"
	"gopkg.in/tucnak/telebot.v2"
)

func initError(err error) {
	logrus.WithFields(logrus.Fields{
		"tag": "initialize error",
		"data": logrus.Fields{
			"err": logrus.Fields{
				"text": err.Error(),
			},
		},
	}).Error()
}

func sendError(err error, response, request *telebot.Message) {
	logrus.WithFields(logrus.Fields{
		"tag": "send-error",
		"data": logrus.Fields{
			"request":  request,
			"response": response,
			"err": logrus.Fields{
				"text": err.Error(),
			},
		},
	}).Error()
}

func logNewCountRequest(msg *telebot.Message) {
	logrus.WithFields(logrus.Fields{
		"tag": "new-count-request",
		"data": logrus.Fields{
			"message": msg,
		},
	}).Info()
}

func validationError(err *ValidationError, msg *telebot.Message) {
	logrus.WithFields(logrus.Fields{
		"tag": "validation-error",
		"data": logrus.Fields{
			"message": msg,
			"err": logrus.Fields{
				"text": err.text,
			},
		},
	}).Info()
}

func internalError(err error, msg *telebot.Message) {
	logrus.WithFields(logrus.Fields{
		"tag": "internal-error",
		"data": logrus.Fields{
			"message": msg,
			"err": logrus.Fields{
				"text": err.Error(),
			},
		},
	}).Error()
}
