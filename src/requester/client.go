package requester

import (
	"net/http"
)

type transportWithHeaders struct {
	tripper http.RoundTripper
}

func (twh *transportWithHeaders) RoundTrip(req *http.Request) (*http.Response, error) {
	//trying to look like real person
	//to get access to some sites with filters (e.g. reddit)
	req.Header.Add("USER-AGENT", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.136 YaBrowser/20.2.2.177 Yowser/2.5 Safari/537.36")
	req.Header.Add("ACCEPT", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
	req.Header.Add("ACCEPT-ENCODING", "gzip, deflate, br")
	req.Header.Add("ACCEPT-LANGUAGE", "en;q=0.9,es;q=0.8")
	return twh.tripper.RoundTrip(req)
}

func newTransportWithHeaders(tripper http.RoundTripper) *transportWithHeaders {
	if tripper == nil {
		tripper = http.DefaultTransport
	}

	return &transportWithHeaders{tripper: tripper}
}
