package requester

import (
	"github.com/sirupsen/logrus"
)

func networkError(err error, url string) {
	logrus.WithFields(logrus.Fields{
		"tag": "requester-network-error",
		"data": logrus.Fields{
			"url": url,
			"err": logrus.Fields{
				"text": err.Error(),
			},
		},
	}).Error()
}

func ioError(err error) {
	logrus.WithFields(logrus.Fields{
		"tag": "requester-io-error",
		"data": logrus.Fields{
			"err": logrus.Fields{
				"text": err.Error(),
			},
		},
	}).Error()
}
