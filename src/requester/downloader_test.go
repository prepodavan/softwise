package requester

import (
	"net/http"
	"net/http/httptest"
	"sync"
	"testing"
)

type handler struct {
	response string
}

func (h *handler) ServeHTTP(rw http.ResponseWriter, req *http.Request) {
	rw.Write([]byte(h.response))
}

func TestDownloader_ContentLength(t *testing.T) {
	h := &handler{"test"}
	server := httptest.NewServer(h)
	defer server.Close()

	d := NewDownloader(server.Client())
	length, err := d.ContentLength(server.URL)
	if err != nil || int(length) != len(h.response) {
		t.Fail()
	}
}

func syncMapContentLength(m *sync.Map) (counter int) {
	m.Range(func(key, value interface{}) bool {
		counter += int(value.(int64))
		return true
	})

	return
}

func TestDownloader_ContentLengthAll(t *testing.T) {
	h := &handler{"test"}
	server := httptest.NewServer(h)
	defer server.Close()

	numThreads := 3
	d := NewDownloader(server.Client())
	results := d.ContentLengthAll(uint64(numThreads), server.URL+"/1", server.URL+"/2", server.URL+"/3")
	if syncMapContentLength(results) != numThreads * len(h.response) {
		t.Fail()
	}
}