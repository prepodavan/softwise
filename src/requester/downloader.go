package requester

import (
	"github.com/miolini/datacounter"
	"io"
	"io/ioutil"
	"net/http"
	"sync"
)

type Downloader struct {
	Client *http.Client
}

func (d *Downloader) ContentLength(url string) (int64, error) {
	res, getErr := d.Client.Get(url)
	if getErr != nil {
		networkError(getErr, url)
		return -1, getErr
	}

	defer res.Body.Close()
	counter := datacounter.NewReaderCounter(res.Body)
	_, err := io.Copy(ioutil.Discard, counter)
	if err != nil && err != io.EOF {
		ioError(err)
		return -1, err
	}

	return int64(counter.Count()), nil
}

func (d *Downloader) ContentLengthAll(numThreads uint64, urls ...string) *sync.Map {
	if numThreads == 1 {
		return d.singleRequest(urls)
	} else if numThreads > 0 {
		ch, wg := d.runSupplier(urls)
		results := d.runRequester(int(numThreads), ch, wg)
		wg.Wait()
		return results
	}

	return &sync.Map{}
}

func NewDownloader(client *http.Client) *Downloader {
	if client == nil {
		client = &http.Client{
			Transport: newTransportWithHeaders(nil),
		}
	}

	return &Downloader{
		Client: client,
	}
}
