package requester

import "sync"

func (d *Downloader) singleRequest(urls []string) (results *sync.Map) {
	results = new(sync.Map)
	for _, url := range urls {
		length, err := d.ContentLength(url)
		if err != nil {
			continue
		}

		results.Store(url, length)
	}

	return
}

func (d *Downloader) runSupplier(contents []string) (<-chan string, *sync.WaitGroup) {
	ch := make(chan string)
	wg := new(sync.WaitGroup)
	wg.Add(1)
	go func() {
		defer wg.Done()
		for _, url := range contents {
			ch <- url
		}
		close(ch)
	}()

	return ch, wg
}

func (d *Downloader) runRequester(num int, ch <-chan string, wg *sync.WaitGroup) (results *sync.Map) {
	wg.Add(num)
	results = new(sync.Map)
	for i := 0; i < num; i++ {
		go func() {
			defer wg.Done()
			for url := range ch {
				length, err := d.ContentLength(url)
				if err != nil {
					continue
				}

				results.Store(url, length)
			}
		}()
	}

	return
}
