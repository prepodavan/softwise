FROM golang:alpine AS downloader

ENV GO111MODULE=on

WORKDIR /app
COPY src/go.mod .
COPY src/go.sum .
RUN cd src/ && go mod download

FROM golang:alpine AS builder

ENV GO111MODULE=on

WORKDIR /app
COPY --from=downloader /go/pkg/mod/cache /go/pkg/mod/cache
COPY --from=downloader /app /app
RUN cd src/ && CGO_ENABLED=0 GOOS=linux go build -ldflags="-w -s"

FROM alpine AS certs
RUN apk --update add ca-certificates

FROM scratch AS last
WORKDIR /root/
COPY --from=builder /app/src/softwise .
COPY --from=certs /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
ENTRYPOINT ["/root/softwise"]